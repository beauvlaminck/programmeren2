﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpLerenViaConsole
{
    class Postcode
    {
        private string code;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        private string plaats;

        public string Plaats
        {
            get { return plaats; }
            set { plaats = value; }
        }

        private string provincie;

        public string Provincie
        {
            get { return provincie; }
            set { provincie = value; }
        }

        private string localite;

        public string Localite
        {
            get { return localite; }
            set { localite = value; }
        }

        private string province;

        public string Province
        {
            get { return province; }
            set { province = value; }
        }

        public string ReadPostcodesFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = "Data\\Postcode.csv";
            // lees het bestand in vanop de harde schijf
            bestand.Lees();
            return bestand.Text;
        }

        public void GetPostcodeList()
        {
            string text = ReadPostcodesFromCSVFile();
            string[] postcodes = text.Split('\n');
        }
    }
}
