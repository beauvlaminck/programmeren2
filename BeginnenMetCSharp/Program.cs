﻿using System;
using Wiskunde.Meetkunde;

namespace CSharpLerenViaConsole
{
   
    class Program
    {

        static void Main(string[] args)
        
        {

            //de driehoek 
           // Console.Write(Vormen.Lijn(0, ConsoleColor.Gray));
            //Console.WriteLine(Vormen.Lijn(6));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(0, '*'));
            //Console.Write(Vormen.Lijn(4, ' '));
           // Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(3, ' '));
           // Console.WriteLine(Vormen.Lijn(2, '*'));
           // Console.Write(Vormen.Lijn(2, ' '));
            //Console.WriteLine(Vormen.Lijn(3, '*'));
           // Console.Write(Vormen.Lijn(1, ' '));
           // Console.WriteLine(Vormen.Lijn(4, '*'));
            //Console.WriteLine(Vormen.Lijn(5, '*'));

            //de rechthoek 
            //Console.Write(Vormen.Lijn(7, ConsoleColor.DarkGreen));
            //Console.Write(Vormen.Lijn(1, '*'));
            
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '*'));
           // Console.Write(Vormen.Lijn(5, ' '));
           // Console.WriteLine(Vormen.Lijn(1, '*'));
           // Console.Write(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '*'));
           // Console.Write(Vormen.Lijn(5, ' '));
           // Console.WriteLine(Vormen.Lijn(1, '*'));
           // Console.WriteLine(Vormen.Lijn(7));
           // Console.WriteLine();

            //Console.ReadKey();
            
            Persoon afzender = new Persoon();
            afzender.voornaam = "Mohamed";
            afzender.familienaam = "El Farisi";
            afzender.Leeftijd = 22;
            Persoon ontvanger = new Persoon();
            ontvanger.voornaam = "Mathilde";
            ontvanger.familienaam = "De Coninck";
            ontvanger.Leeftijd = 24;
            Persoon ontvanger2 = new Persoon();
            ontvanger2.voornaam = "Jan";
            ontvanger2.familienaam = "Dewilde";
            ontvanger2.Leeftijd = 23;
            Persoon ontvanger3 = new Persoon();
            ontvanger3.voornaam = "Ilse";
            ontvanger3.familienaam = "Peeters";
            ontvanger3.Leeftijd = 286;

            string afzendertekst = afzender.ShowInfo();
            Console.WriteLine(afzendertekst);
            string ontvangertekst = ontvanger.ShowInfo();
            Console.WriteLine(ontvangertekst);
            string ontvanger2tekst = ontvanger2.ShowInfo();
            Console.WriteLine(ontvanger2tekst);
            string ontvanger3tekst = ontvanger3.ShowInfo();
            Console.WriteLine(ontvanger3tekst);
            Console.ReadKey();
        }


    }
}
