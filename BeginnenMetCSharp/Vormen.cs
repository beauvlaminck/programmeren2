﻿using System;
using System.Reflection;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value; // value is een implicatiete parameter van een setter 
                Console.ForegroundColor = kleur;
            }
        }

        public static string Lijn(int lengte)
        {
            return new string('_', lengte);

        }

        public static string Lijn(int lengte, char teken)
        {
            return new string(teken, lengte);

        }
        public static string Lijn(int lengte,ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;
            return new string('_', lengte);
        }
        public static string Driehoek(int lengte, char teken,ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;
            return new string(teken,lengte);
        }

    }
}